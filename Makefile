.PHONY: install
install:
	go mod tidy
	go mod download

.PHONY: build-client
build-client:
	go build -o bin/client ./cmd/client

.PHONY: build-server
build-server:
	go build -o bin/server ./cmd/server

.PHONY: run-server
run-server: build-server
	./bin/server

.PHONY: run-client
run-client: build-client
	./bin/client

.PHONY: test
test:
	go test ./pkg/...

.PHONY: test-rdate
test-rdate:
	rdate -p localhost
	rdate -u -p localhost

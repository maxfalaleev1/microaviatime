# oneline

build and run server

```shell
make install && make build-server && make build-client
make run-server
```

run client and test with rdate

```shell
make run-client && make test-rdate
```

## install

```shell
make install
```

## build server

```shell
make build-server
```

## build client

```shell
make build-client
```

## run tests

```shell
make test
```

## run server

```shell
make run-server
```

## run client

```shell
make run-client
```

## run rdate

```shell
make test-rdate
```

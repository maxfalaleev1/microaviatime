package main

import (
	"aviatime/pkg/timesrv"
	"context"
	"flag"
	"github.com/rs/zerolog/log"
	"os"
	"os/signal"
	"syscall"
)

var address = flag.String("address", "0.0.0.0", "listen addr (default: 0.0.0.0)")
var port = flag.Int("port", 37, "application port (default: 37)")

func main() {
	flag.Parse()

	ctx, cancel := context.WithCancel(context.Background())
	srv := timesrv.NewServer(*address, *port)

	go func() {
		if err := srv.ListenTCP(ctx); err != nil {
			log.Fatal().Err(err).Msg("srv.ListenTCP")
		}
	}()

	go func() {
		if err := srv.ListenUDP(ctx); err != nil {
			log.Fatal().Err(err).Msg("srv.ListenUDP")
		}
	}()

	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, syscall.SIGTERM, syscall.SIGINT)

	<-signalCh
	log.Info().Msg("signal received")
	cancel()
}

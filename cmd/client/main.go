package main

import (
	"aviatime/pkg/timesrv"
	"flag"
	"github.com/rs/zerolog/log"
	"sync"
)

var address = flag.String("address", "0.0.0.0", "listen addr (default: 0.0.0.0)")
var port = flag.Int("port", 37, "application port (default: 37)")

func main() {
	flag.Parse()

	timeClient := timesrv.NewClient(*address, *port)

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		t, err := timeClient.SendUDP()
		if err != nil {
			log.Fatal().Err(err).Msg("timeClient.SendUDP")
		}
		log.Info().Msgf("UDP %s", t)
		wg.Done()
	}()

	go func() {
		t, err := timeClient.SendTCP()
		if err != nil {
			log.Fatal().Err(err).Msg("timeClient.SendTCP")
		}
		log.Info().Msgf("TCP %s", t)
		wg.Done()
	}()

	wg.Wait()
}

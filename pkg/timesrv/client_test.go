package timesrv

import (
	"context"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestClientImpl_SendTCP(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	srv := NewServer("0.0.0.0", 37)
	go func() {
		require.NoError(t, srv.ListenTCP(ctx))
	}()
	time.Sleep(1 * time.Second)
	c := NewClient("localhost", 37)
	resp, err := c.SendTCP()
	require.NoError(t, err)
	require.NotNil(t, resp)
	year, month, day := resp.Date()
	now := time.Now()
	require.Equal(t, year, now.Year())
	require.Equal(t, month, now.Month())
	require.Equal(t, day, now.Day())
	cancel()
}

func TestClientImpl_SendUDP(t *testing.T) {
	ctx, cancel := context.WithCancel(context.TODO())
	srv := NewServer("0.0.0.0", 37)
	go func() {
		require.NoError(t, srv.ListenUDP(ctx))
	}()
	time.Sleep(1 * time.Second)
	c := NewClient("localhost", 37)
	resp, err := c.SendUDP()
	require.NoError(t, err)
	require.NotNil(t, resp)
	year, month, day := resp.Date()
	now := time.Now()
	require.Equal(t, year, now.Year())
	require.Equal(t, month, now.Month())
	require.Equal(t, day, now.Day())
	cancel()
}

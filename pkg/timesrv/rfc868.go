package timesrv

import (
	"bytes"
	"encoding/binary"
	"time"
)

const timeOffset int64 = 2208988800 // time at 00:00:00 1 Jan 1970 GMT

func RFC868Time() uint32 {
	return uint32((time.Now().Unix() + timeOffset) & 0xFFFFFFFF)
}

// Time The time is the number of seconds since 00:00 (midnight) 1 January 1900 GMT, such that the time 1 is 12:00:01 am on 1 January 1900 GMT; this base will serve until the year 2036.
// For example:
//	the time  2,208,988,800 corresponds to 00:00  1 Jan 1970 GMT,
//  2,398,291,200 corresponds to 00:00  1 Jan 1976 GMT,
//  2,524,521,600 corresponds to 00:00  1 Jan 1980 GMT,
//  2,629,584,000 corresponds to 00:00  1 May 1983 GMT,
//  and -1,297,728,000 corresponds to 00:00 17 Nov 1858 GMT.
func Time() ([]byte, error) {
	buf := new(bytes.Buffer)
	defer buf.Reset()

	err := binary.Write(buf, binary.BigEndian, RFC868Time())
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func FromTime(buf []byte) (*time.Time, error) {
	var t uint32
	err := binary.Read(bytes.NewReader(buf), binary.BigEndian, &t)
	if err != nil {
		return nil, err
	}

	res := time.Unix(int64(t)-timeOffset, 0)
	return &res, nil
}

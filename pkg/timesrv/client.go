package timesrv

import (
	"net"
	"time"
)

type clientImpl struct {
	port    int
	address net.IP
}

func (c *clientImpl) zeroBuf() []byte {
	return make([]byte, 0)
}

func (c *clientImpl) SendTCP() (*time.Time, error) {
	ct, err := net.DialTCP("tcp", nil, &net.TCPAddr{IP: c.address, Port: c.port})
	if err != nil {
		return nil, err
	}

	defer func() {
		if err := ct.Close(); err != nil {
			panic(err)
		}
	}()

	if _, err := ct.Write(c.zeroBuf()); err != nil {
		return nil, err
	}

	buf := make([]byte, 4)
	nn, err := ct.Read(buf)
	if err != nil {
		return nil, err
	}

	return FromTime(buf[:nn])
}

func (c *clientImpl) SendUDP() (*time.Time, error) {
	cu, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: c.address, Port: c.port})
	if err != nil {
		return nil, err
	}

	defer func() {
		if err := cu.Close(); err != nil {
			panic(err)
		}
	}()

	if _, err := cu.Write(c.zeroBuf()); err != nil {
		return nil, err
	}

	buf := make([]byte, 4)
	nn, err := cu.Read(buf)
	if err != nil {
		return nil, err
	}

	return FromTime(buf[:nn])
}

type TimeClient interface {
	SendTCP() (*time.Time, error)
	SendUDP() (*time.Time, error)
}

func NewClient(address string, port int) TimeClient {
	return &clientImpl{
		address: net.ParseIP(address),
		port:    port,
	}
}

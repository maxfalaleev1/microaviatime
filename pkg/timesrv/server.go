package timesrv

import (
	"context"
	"github.com/rs/zerolog/log"
	"net"
)

type TimeServer interface {
	ListenTCP(ctx context.Context) error
	ListenUDP(ctx context.Context) error
}

type timeServerImpl struct {
	port    int
	address net.IP
}

func (t *timeServerImpl) ListenTCP(ctx context.Context) error {
	listen, err := net.ListenTCP("tcp", &net.TCPAddr{Port: t.port, IP: t.address})
	if err != nil {
		log.Err(err).Msg("net.ListenTCP")
		return err
	}

	defer func() {
		err := listen.Close()
		log.Err(err).Msg("listen.Close")
	}()

	log.Info().Msg("ListenTCP")

	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			c, err := listen.Accept()
			if err != nil {
				log.Err(err).Msg("listen.Accept")
				return err
			}

			go func(c net.Conn) {
				defer func() {
					err := c.Close()
					log.Err(err).Msg("c.Close")
				}()

				resp, err := Time()
				if err != nil {
					log.Err(err).Msg("rfc868time.Time")
					return
				}

				if _, err := c.Write(resp); err != nil {
					log.Err(err).Msg("c.Write")
				}
			}(c)
		}
	}
}

func (t *timeServerImpl) ListenUDP(ctx context.Context) error {
	conn, err := net.ListenUDP("udp", &net.UDPAddr{Port: t.port, IP: t.address})
	if err != nil {
		log.Err(err).Msg("net.ListenUDP")
		return err
	}

	defer func() {
		err := conn.Close()
		log.Err(err).Msg("conn.Close")
	}()

	log.Info().Msg("ListenUDP")

	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			buffer := make([]byte, 1500)
			_, addr, err := conn.ReadFromUDP(buffer)
			if err != nil {
				log.Info().Msg("conn.ReadFromUDP")
				continue
			}

			resp, err := Time()
			if err != nil {
				log.Err(err).Msg("rfc868time.Time")
				continue
			}

			_, err = conn.WriteToUDP(resp, addr)
			if err != nil {
				log.Err(err).Msg("conn.WriteToUDP")
				continue
			}
		}
	}
}

func NewServer(address string, port int) TimeServer {
	return &timeServerImpl{
		address: net.ParseIP(address),
		port:    port,
	}
}
